# Diretório de Notebooks - Instruções Demo Outreach

Este diretório contém notebooks Jupyter associados aos projetos de pesquisa. Cada notebook disponibilizado aqui será utilizado para exibição e interação através do site [reg.icmc.usp.br](http://reg.icmc.usp.br), permitindo que os usuários executem rotinas interativas diretamente no navegador.
  
Para garantir que cada notebook funcione corretamente com suas dependências específicas, cada pesquisador deve seguir as instruções abaixo ao adicionar seus arquivos.

## Estrutura do Diretório

Cada notebook deve estar em um **diretório separado** contendo o próprio arquivo `.ipynb` e um arquivo de dependências (`requirements.txt`).


```
/notebooks/
├── projeto1/
│   ├── notebook_projeto1.ipynb
│   ├── requirements.txt  (ou environment.yml)
├── projeto2/
│   ├── notebook_projeto2.ipynb
│   ├── requirements.txt  (ou environment.yml)
```

## Passos para Subir um Notebook

1. **Criar diretório**: Cada notebook deve ter seu próprio diretório dentro de `/notebooks/`.
2. **Adicionar o arquivo `.ipynb`**: Certifique-se de que o notebook esteja funcionando corretamente.
3. **Incluir arquivo de dependências**:
   - Use `requirements.txt` para listar as bibliotecas Python necessárias.

### Exemplo de `requirements.txt`:
```txt
numpy
pandas
matplotlib
```

## Teste

Antes de subir, verifique localmente se o notebook funciona com as dependências listadas.
  
---

Se precisar de ajuda, entre em contato com a equipe de suporte do projeto.
